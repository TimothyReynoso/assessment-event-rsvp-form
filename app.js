const express = require("express");
const mongoose = require("mongoose");
const path = require("path");
const pug = require("pug");
require('dotenv/config')

const app = express();
const paths = process.env.DB_CONNECT
//app.use(express.json())
app.use(
  express.urlencoded())


// // getting-started.js

mongoose.connect(paths, {useNewUrlParser: true, useUnifiedTopology: true });

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  // we're connected!
  
});

//creating our database Schema or our "upload standards"
const responseSchema =  mongoose.Schema({ 
    name: String,
    email: String, 
    attending: Boolean,
    guests: Number

})

//.model('collection we want to use', the Schema we want to follow)
const Response = mongoose.model('responses', responseSchema);



//===================================================================================
//app.set("view engine", "express")
app.set("view engine", "pug")

//app.use(express.static("./views"))

app.get("/", (req, res)=> {res.render("index.pug")});

app.get("/guests", (req, res)=> {
    // const doc = Response.find({attending: true});
    // console.log(doc)
    Response.find((err, data)=> {
        let attendingData = data.map((groupList)=> {
            if(groupList.attending===true){
                return groupList
            }
        })
        let notAttendingData = data.map((groupList)=> {
            if(groupList.attending===false){
                return groupList
            }
        })
        
        //console.log(attendingData)
        if(err){
            return console.error(err);
        }else{
            res.render("guests.pug", {
                attending: attendingData,
                notAttending: notAttendingData
            })
        }

        })
    })

   


app.post("/replay", (req, res)=> {
    data = req.body
    // // NOTE: methods must be added to the schema before compiling it with mongoose.model()
    const datas = new Response(data)
    datas.save()
    res.render("replay.pug")
});

const port = 3001

app.listen(port, ()=>{
    console.log(`Server started on port ${port}`);
})


// // // getting-started.js
// const mongoose = require('mongoose');
// mongoose.connect('mongodb://localhost/test', {useNewUrlParser: true});

// const db = mongoose.connection;
// db.on('error', console.error.bind(console, 'connection error:'));
// db.once('open', function() {
//   // we're connected!
//   console.log(`we are connected`)
// });

// const kittySchema = new mongoose.Schema(
//     { name: String }
// )

// // NOTE: methods must be added to the schema before compiling it with mongoose.model()
// kittySchema.methods.speak = function () {
//     const greeting = this.name 
//         ? "Meow name is " + this.name //if true
//         : "I don't have a name";     //if false
//     console.log(greeting)
// }

// const Kitten = mongoose.model('Kitten', kittySchema);

// //giving a kitten named silence 
// const silence = new Kitten({name: "Silence"});
//     console.log(silence.name);

// const fluffy = new Kitten({ name: 'fluffy' });
//     fluffy.speak();

// fluffy.save(function (err, fluffy){
//     if(err) {
//         return console.error(err);
//     } else {
//         fluffy.speak();
//     }
// })

// //should hook up to the mongoose database
// Kitten.find(function(err, kittens){
//     if(err) {
//         return console.error(err) //not really needed
//     } else {
//         console.log(kittens);
//     }
// })



